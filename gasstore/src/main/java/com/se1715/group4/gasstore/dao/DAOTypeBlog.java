/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.se1715.group4.gasstore.dao;

import com.se1715.group4.gasstore.dto.Product;
import com.se1715.group4.gasstore.dto.TypeBlog;
import com.se1715.group4.gasstore.util.DBUtil;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ADMIN
 */
public class DAOTypeBlog extends DBUtil {

    public ArrayList<TypeBlog> getAllTypeBlogs() {
        ArrayList<TypeBlog> blogs = new ArrayList<>();
        String sql = "SELECT * FROM TypeBlog";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int typeID = rs.getInt("typeID");
                String typeName = rs.getString("typeName");
                String createdDate = rs.getString("createdDate");
                int createdBy = rs.getInt("createdBy");
                blogs.add(new TypeBlog(typeID, typeName, createdDate, createdBy));

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return blogs;
    }

    public Map<Integer, TypeBlog> getMapTypeBlog() {
        Map<Integer, TypeBlog> mapTypeBlog = new HashMap<>();
        String sql = "select * from typeblog";
        try {
            ResultSet rs = getData(sql);

            while (rs.next()) {
                int typeId = rs.getInt("typeId");
                String typeName = rs.getString("typeName");
                String createdDate = rs.getString("createdDate");
                int createdBy = rs.getInt("createdBy");
                mapTypeBlog.put(typeId, new TypeBlog(typeId, typeName, createdDate, createdBy));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return mapTypeBlog;
    }

    public static void main(String[] args) {
        DAOTypeBlog daotypeblog = new DAOTypeBlog();
        ArrayList<TypeBlog> blogs = daotypeblog.getAllTypeBlogs();

        daotypeblog = new DAOTypeBlog();
        

    }
}
