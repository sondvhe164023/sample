/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.se1715.group4.gasstore.dao;

import com.se1715.group4.gasstore.dto.Blog;
import com.se1715.group4.gasstore.dto.Customer;
import com.se1715.group4.gasstore.dto.TypeBlog;
import com.se1715.group4.gasstore.util.DBUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ADMIN
 */
public class DAOBlog extends DBUtil {

    private final Connection connection = DBUtil.makeConnection();

    public static void main(String[] args) {
        DAOBlog dao = new DAOBlog();
        ArrayList<Blog> blogs = dao.getAllBlogs();

        for (Blog blog : blogs) {
            System.out.println(blog);
        }
    }
    
    public ArrayList<Blog> getListBlogByPage(ArrayList<Blog> list, int start, int end) {
        ArrayList<Blog> arr = new ArrayList<>();
        for (int i = start; i < end; ++i) {
            arr.add(list.get(i));
        }
        return arr;
    }

    public ArrayList<Blog> getAllBlogs() {
        ArrayList<Blog> blogs = new ArrayList<>();
        String sql = "select * from blog order by [datePost]  Desc";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int blogId = rs.getInt("blogId");
                int adminId = rs.getInt("adminId");
                int typeBlog = rs.getInt("typeBlogId");
                String datePost = rs.getString("datePost");
                String code = rs.getString("code");
                String lastChange = rs.getString("lastChange");
                boolean status = rs.getBoolean("status");
                String title = rs.getString("title");
                String headerContent = rs.getString("headerContent");
                String quoteContent = rs.getString("quoteContent");
                String bodyContent = rs.getString("bodyContent");
                String footerContent = rs.getString("footerContent");
                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    blogs.add(new Blog(blogId, code,adminId, typeBlog, datePost, lastChange, status, title, headerContent, quoteContent, bodyContent, footerContent, base64Image));

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return blogs;
    }

    public ArrayList<Blog> getBlogsByType(int typeBlogId) {
        ArrayList<Blog> blogs = new ArrayList<>();
        String sql = "select * from blog where typeBlogId =" + typeBlogId + "order by [datePost]  Desc";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int blogId = rs.getInt("blogId");
                int adminId = rs.getInt("adminId");
                int typeBlog = rs.getInt("typeBlogId");
                String datePost = rs.getString("datePost");
                String code = rs.getString("code");
                String lastChange = rs.getString("lastChange");
                boolean status = rs.getBoolean("status");
                String title = rs.getString("title");
                String headerContent = rs.getString("headerContent");
                String quoteContent = rs.getString("quoteContent");
                String bodyContent = rs.getString("bodyContent");
                String footerContent = rs.getString("footerContent");
                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    blogs.add(new Blog(blogId, code,adminId, typeBlog, datePost, lastChange, status, title, headerContent, quoteContent, bodyContent, footerContent, base64Image));

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return blogs;
    }

    public ArrayList<Blog> getBlogsByMonth(int month) {
        ArrayList<Blog> blogs = new ArrayList<>();
        String sql = "select * from blog where month(datePost) = " + month + "order by [datePost]  Desc";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int blogId = rs.getInt("blogId");
                int adminId = rs.getInt("adminId");
                int typeBlog = rs.getInt("typeBlogId");
                String datePost = rs.getString("datePost");
                String code = rs.getString("code");
                String lastChange = rs.getString("lastChange");
                boolean status = rs.getBoolean("status");
                String title = rs.getString("title");
                String headerContent = rs.getString("headerContent");
                String quoteContent = rs.getString("quoteContent");
                String bodyContent = rs.getString("bodyContent");
                String footerContent = rs.getString("footerContent");
                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    blogs.add(new Blog(blogId, code,adminId, typeBlog, datePost, lastChange, status, title, headerContent, quoteContent, bodyContent, footerContent, base64Image));

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return blogs;
    }

    public Map<Integer, Integer> getBlogTypeNumber() {
        Map<Integer, Integer> categoryNumber = new HashMap<>();
        String sql = "select typeBlogId, count(*) from Blog group by typeBlogId";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int typeBlog = rs.getInt(1);
                int number = rs.getInt(2);
                categoryNumber.put(typeBlog, number);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return categoryNumber;
    }

    public Map<Integer, Integer> getDateNumberBlog() {
        Map<Integer, Integer> dateNumberBlog = new HashMap<>();
        String sql = "select MONTH(datePost), COUNT(*) from blog group by MONTH(datePost)";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int month = rs.getInt(1);
                int number = rs.getInt(2);
                dateNumberBlog.put(month, number);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return dateNumberBlog;
    }

    public ArrayList<Integer> getlistMonth() {
        ArrayList<Integer> dateNumberBlog = new ArrayList<>();
        String sql = "select distinct MONTH(datePost) as month from blog order by month desc";
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int month = rs.getInt(1);;
                dateNumberBlog.add(month);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return dateNumberBlog;
    }

    public Blog getBlogById(int id) {
        Blog blog = new Blog();
        String sql = "select * from blog where BlogId =" + id;
        ResultSet rs = getData(sql);
        try {
            while (rs.next()) {
                int blogId = rs.getInt("blogId");
                int adminId = rs.getInt("adminId");
                int typeBlog = rs.getInt("typeBlogId");
                String datePost = rs.getString("datePost");
                String lastChange = rs.getString("lastChange");
                boolean status = rs.getBoolean("status");
                String title = rs.getString("title");
                String code = rs.getString("code");
                String headerContent = rs.getString("headerContent");
                String quoteContent = rs.getString("quoteContent");
                String bodyContent = rs.getString("bodyContent");
                String footerContent = rs.getString("footerContent");

                Blob blob = rs.getBlob("img");

                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    blog = new Blog(blogId, code,adminId, typeBlog, datePost, lastChange, status, title, headerContent, quoteContent, bodyContent, footerContent, base64Image);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return blog;
    }
//
//    public ArrayList<Blog> getListBlogByPage(ArrayList<Blog> list, int start, int end) {
//        ArrayList<Blog> arr = new ArrayList<>();
//        for (int i = start; i < end; ++i) {
//            arr.add(list.get(i));
//        }
//        return arr;
//    }

    
}
