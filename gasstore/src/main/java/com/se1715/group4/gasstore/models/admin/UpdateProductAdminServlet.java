/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package com.se1715.group4.gasstore.models.admin;

import com.se1715.group4.gasstore.dao.*;
import com.se1715.group4.gasstore.dto.Category;
import com.se1715.group4.gasstore.dto.Product;
import com.se1715.group4.gasstore.dto.Supplier;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.HttpServletResponse;
import java.util.*;
/**
 *
 * @author ADMIN
 */
@MultipartConfig(maxFileSize = 16177215)
public class UpdateProductAdminServlet extends HttpServlet {
   
    private static final long serialVersionUID = 1L;
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateProductAdminServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateProductAdminServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String productID_raw = request.getParameter("pid");
        DAOProduct daoProducts = new DAOProduct();
        DAOSupplier daoSupplier = new DAOSupplier();
        DAOCategory daoCategories = new DAOCategory();
        String id_raw = request.getParameter("imgid");
        try {
            int productID = Integer.parseInt(productID_raw);

//            Product product = daoProducts.getProductByID(productID);
//            Vector<Category> listAllCate = daoCategories.getAllCategories();
            Vector<Supplier> listAllSup = daoSupplier.getAllSuppliers("");

//            if (id_raw != null) {
//                int id = Integer.parseInt(id_raw);
//                for (int i = 0; i < listAllImage.size(); i++) {
//                    if (listAllImage.get(i).getId() == id) {
//                        listAllImage.remove(i);
//                        daoProImg.DeleteProductImg(id);
//                    }
//                }
//
//            }
//            listAllImage = daoProductImage.getAllImageProductByProductID(productID);
//            request.setAttribute("product", product);
//            request.setAttribute("listAllImage", listAllImage);
//            request.setAttribute("listAllCate", listAllCate);
//            request.setAttribute("listAllSup", listAllSup);

            request.getRequestDispatcher("updateproduct.jsp").forward(request, response);
        } catch (Exception e) {
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
