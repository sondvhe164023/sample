/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.se1715.group4.gasstore.dto;


/**
 *
 * @author ADMIN
 */
public class Blog {
    private int blogId;
    private String code;
    private int adminId;
    private int typeBlogId;
    private String datePost;
    private String lastChange;
    private boolean status;
    private String title;
    private String headerContent;
    private String quoteContent;
    private String bodyContent;
    private String footerContent;
    private String base64Image;

    public Blog(int blogId,String code, int adminId, int typeBlogId, String datePost, String lastChange, boolean status, String title, String headerContent, String quoteContent, String bodyContent, String footerContent, String base64Image) {
        this.blogId = blogId;
        this.adminId = adminId;
        this.code = code;
        this.typeBlogId = typeBlogId;
        this.datePost = datePost;
        this.lastChange = lastChange;
        this.status = status;
        this.title = title;
        this.headerContent = headerContent;
        this.quoteContent = quoteContent;
        this.bodyContent = bodyContent;
        this.footerContent = footerContent;
        this.base64Image = base64Image;
    }

    public Blog() {
    }

    public int getBlogId() {
        return blogId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public int getTypeBlogId() {
        return typeBlogId;
    }

    public void setTypeBlogId(int typeBlogId) {
        this.typeBlogId = typeBlogId;
    }

    public String getDatePost() {
        return datePost;
    }

    public void setDatePost(String datePost) {
        this.datePost = datePost;
    }

    public String getLastChange() {
        return lastChange;
    }

    public void setLastChange(String lastChange) {
        this.lastChange = lastChange;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHeaderContent() {
        return headerContent;
    }

    public void setHeaderContent(String headerContent) {
        this.headerContent = headerContent;
    }

    public String getQuoteContent() {
        return quoteContent;
    }

    public void setQuoteContent(String quoteContent) {
        this.quoteContent = quoteContent;
    }

    public String getBodyContent() {
        return bodyContent;
    }

    public void setBodyContent(String bodyContent) {
        this.bodyContent = bodyContent;
    }

    public String getFooterContent() {
        return footerContent;
    }

    public void setFooterContent(String footerContent) {
        this.footerContent = footerContent;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

}
