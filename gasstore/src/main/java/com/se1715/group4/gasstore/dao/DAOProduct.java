
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.se1715.group4.gasstore.dao;

import com.se1715.group4.gasstore.dto.Category;
import com.se1715.group4.gasstore.dto.Discount;
import com.se1715.group4.gasstore.dto.Product;
import com.se1715.group4.gasstore.dto.Review;
import com.se1715.group4.gasstore.dto.Supplier;
import com.se1715.group4.gasstore.util.DBUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class DAOProduct {

    public static void main(String[] args) {
        DAOProduct dao = new DAOProduct();
        Vector<Product> vector = dao.SearchProduct("a");
        for (Product product : vector) {
            System.out.println(product);
        }
        
    }

    private final Connection connection = DBUtil.makeConnection();

    public Vector<Product> SearchProduct(String key){
        DAOSupplier daoSupplier = new DAOSupplier();
        DAOCategory daoCategory = new DAOCategory();
        DAODiscount daoDiscount = new DAODiscount();
        Vector<Product> vector = new Vector<>();
        String sql = "SELECT * FROM Product WHERE name like ? OR code like ? ";
        key = "%" + key+ "%";
        
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, key);
            pre.setString(2, key);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                int productId = rs.getInt("productID");
                String code = rs.getString("code");
                String name = rs.getString("name");
                double unitPrice = rs.getDouble("unitPrice");
                int disId = rs.getInt("discountId");
                int warranty = rs.getInt("warranty");
                int cateId = rs.getInt("categoryId");
                Blob blob = rs.getBlob("Image");
                Discount discount = daoDiscount.GetDiscountById(disId);
                Category category = daoCategory.GetCategoryById(cateId);
                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    vector.add(new Product(productId, code, name, unitPrice, base64Image, discount, category));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return vector;
        
    }
    
    
    public Product getInformationProduct(int id) {
        String sql = "SELECT * FROM PRODUCT WHERE PRODUCTID = ?";
        Product p = null;
        DAOCategory daoCategory = new DAOCategory();
        DAODiscount daoDiscount = new DAODiscount();
        DAOSupplier daoSupplier = new DAOSupplier();
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                String code = rs.getString("code");
                String name = rs.getString("name");
                String shortDescription = rs.getString("shortDescription");
                String description = rs.getString("description");
                int categoryID = rs.getInt("categoryID");
                int supplierId = rs.getInt("supplierId");
                int stockQuantity = rs.getInt("stockQuantity");
                int discountId = rs.getInt("discountId");
                int warranty = rs.getInt("warranty");
                double unitPrice = rs.getDouble("unitPrice");
                Blob blob = rs.getBlob("Image");
                Discount discount = daoDiscount.GetDiscountById(discountId);
                Category category = daoCategory.GetCategoryById(categoryID);
                Supplier supplier = daoSupplier.getSuppliersBySupplierID(supplierId);
                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    p = new Product(id, code, name, code, shortDescription, description, true, unitPrice, base64Image, stockQuantity, category, supplier, warranty, discount);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        return p;
    }

    public int getProductIdByCode(String code) {
        int id = 0;
        String sql = "SELECT productId from PRoduct where code = ?";

        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setString(1, code);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                id = rs.getInt("productId");
            }
        } catch (SQLException ex) {
        }

        return id;
    }

    public Vector<Product> getProductsBySuppliers(int[] id, int cId, String idorder, double from, double to) {
        DAOCategory daoCategory = new DAOCategory();
        DAODiscount daoDiscount = new DAODiscount();
        DAOSupplier daoSupplier = new DAOSupplier();
        Vector<Product> vector = new Vector<>();
        String sql = "SELECT p.*, discount FROM Product p inner join discount on discount.discountId = p.discountId WHERE p.isActive = 1 AND CategoryID = ? ";
        if (id != null) {
            sql += "AND p.SupplierID IN(";
            for (int i = 0; i < id.length; i++) {
                sql += id[i] + ",";
            }
            if (sql.endsWith(",")) {
                sql = sql.substring(0, sql.length() - 1);
            }
            sql += ") ";
        }
        if (from >= 0 || to <= getTopPrice("DESC")) {
            sql = sql + "AND UnitPrice BETWEEN " + from + " AND " + to;
        }
        String orderby = "Name";
        if (idorder == null || idorder.equals("1")) {
            orderby = " Name ASC";
        }

        if (idorder == null || idorder.equals("2")) {
            orderby = " Name DESC";
        }

        if (idorder.equals("3")) {
            orderby = " (unitPrice-unitPrice*discount) ASC";
        }
        if (idorder.equals("4")) {
            orderby = " (unitPrice-unitPrice*discount) DESC";
        }

        sql = sql + " ORDER BY " + orderby;

        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, cId);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                String code = rs.getString("code");
                String name = rs.getString("name");
                String shortDescription = rs.getString("shortDescription");
                String description = rs.getString("description");
                int categoryID = rs.getInt("categoryID");
                int productId = rs.getInt("productID");
                int supplierId = rs.getInt("supplierId");
                int stockQuantity = rs.getInt("stockQuantity");
                int discountId = rs.getInt("discountId");
                int warranty = rs.getInt("warranty");
                double unitPrice = rs.getDouble("unitPrice");
                Blob blob = rs.getBlob("Image");
                Discount discount = daoDiscount.GetDiscountById(discountId);
                Category category = daoCategory.GetCategoryById(categoryID);
                Supplier supplier = daoSupplier.getSuppliersBySupplierID(supplierId);
                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    vector.add(new Product(productId, code, name, code, shortDescription, description, true, unitPrice, base64Image, stockQuantity, category, supplier, warranty, discount));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vector;
    }

    public double getTopPrice(String type) {
        double maxPrice = 0;
        String sql = "SELECT TOP 1 UnitPrice FROM Product Order by Unitprice " + type;
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                maxPrice = rs.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return maxPrice;
    }

    public Vector<Product> getListByPage(Vector<Product> vector,
            int start, int end) {
        Vector<Product> arr = new Vector<>();
        for (int i = start; i < end; i++) {
            arr.add(vector.get(i));
        }
        return arr;
    }

    public Vector<Product> getProductByCategory(int cateId, String status) {
        DAOSupplier daoSupplier = new DAOSupplier();
        DAOCategory daoCategory = new DAOCategory();
        DAODiscount daoDiscount = new DAODiscount();
        Vector<Product> vector = new Vector<>();
        String sql = "SELECT * FROM Product WHERE CategoryId = ? ";
        if (!status.isEmpty()) {
            sql += status;
        }
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, cateId);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                int productId = rs.getInt("productID");
                String code = rs.getString("code");
                String name = rs.getString("name");
                double unitPrice = rs.getDouble("unitPrice");
                int disId = rs.getInt("discountId");
                int warranty = rs.getInt("warranty");
                Blob blob = rs.getBlob("Image");
                Discount discount = daoDiscount.GetDiscountById(disId);
                Category category = daoCategory.GetCategoryById(cateId);
                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    vector.add(new Product(productId, code, name, unitPrice, base64Image, discount, category));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return vector;
    }

    public Product getProductById(int id) {
        Product p = new Product();
        String sql = "SELECT code, name, unitPrice from product where productId = ?";

        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                p.setProductId(id);
                p.setName(rs.getString("name"));
                p.setCode(rs.getString("code"));
                p.setUnitPrice(rs.getDouble("unitPrice"));
            }
        } catch (SQLException ex) {
        }
        return p;
    }

    public Vector<Product> getAllProducts() throws IOException {
        Vector<Product> product = new Vector<>();
        DAODiscount daoDiscount = new DAODiscount();
        DAOCategory daoCategory = new DAOCategory();
        DAOSupplier daoSupplier = new DAOSupplier();
        String sql = "SELECT * FROM Product";
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            ResultSet rs = pre.executeQuery();
            while (rs.next()) {
                int productId = rs.getInt("productId");
                String code = rs.getString("code");
                String name = rs.getString("name");
                String shortDescription = rs.getString("shortDescription");
                String description = rs.getString("description");
                int categoryID = rs.getInt("categoryID");
                int supplierId = rs.getInt("supplierId");
                int stockQuantity = rs.getInt("stockQuantity");
                int discountId = rs.getInt("discountId");
                int warranty = rs.getInt("warranty");
                double unitPrice = rs.getDouble("unitPrice");
                Blob blob = rs.getBlob("Image");
                Discount discount = daoDiscount.GetDiscountById(discountId);
                Category category = daoCategory.GetCategoryById(categoryID);
                Supplier supplier = daoSupplier.getSuppliersBySupplierID(supplierId);
                InputStream inputStream = blob.getBinaryStream();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                try {
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = outputStream.toByteArray();
                    String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                    inputStream.close();
                    outputStream.close();
                    product.add(new Product(productId, code, name, code, shortDescription, description, true, unitPrice, base64Image, stockQuantity, category, supplier, warranty, discount));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return product;
    }

    public Product getProduct(int ID) {
        Product p = null;
        String sql = "SELECT * FROM Product WHERE productId = ?";
        DAOSupplier daoSup = new DAOSupplier();
        DAOCategory daoCate = new DAOCategory();
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, ID);
            ResultSet rs = pre.executeQuery();
            if (rs.next()) {
                String name = rs.getString("name");
                int cateId = rs.getInt("CategoryId");
                int supId = rs.getInt("SupplierID");
                int disId = rs.getInt("discountId");
                Supplier s = daoSup.getSuppliersBySupplierID(supId);
                Category c = daoCate.GetCategoryById(cateId);
                p = new Product(supId, name, c, s);
            }
        } catch (SQLException ex) {
        }

        return p;
    }

}
