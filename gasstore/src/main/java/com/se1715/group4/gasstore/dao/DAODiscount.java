/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.se1715.group4.gasstore.dao;

import com.se1715.group4.gasstore.dto.Discount;
import com.se1715.group4.gasstore.util.DBUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author ADMIN
 */
public class DAODiscount {
    private final Connection connection = DBUtil.makeConnection();
    
    public Discount GetDiscountById(int id){
        Discount dis = null;
        
        String sql = "SELECT * FROM Discount WHERE DiscountId =  ?";
        
        try {
            PreparedStatement pre = connection.prepareStatement(sql);
            pre.setInt(1, id);
            ResultSet rs = pre.executeQuery();
            if(rs.next()){
                String name = rs.getString("name");
                double discount = rs.getDouble("discount");
                boolean status = rs.getBoolean("isActive");
                dis = new Discount(id, name, status, discount);
            }
        } catch (SQLException ex) {
        }
        
        return dis;
    }
}
