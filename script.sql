USE [master]
GO

-- DROP DATABASE  [SE1715_G4_SWP391]
/****** Object:  Database [SE1715_G4_SWP391]    Script Date: 5/17/2023 3:32:32 PM ******/
CREATE DATABASE [SE1715_G4_SWP391]
ALTER DATABASE [SE1715_G4_SWP391] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SE1715_G4_SWP391].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SE1715_G4_SWP391] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET ARITHABORT OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET  MULTI_USER 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SE1715_G4_SWP391] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SE1715_G4_SWP391] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'SE1715_G4_SWP391', N'ON'
GO
ALTER DATABASE [SE1715_G4_SWP391] SET QUERY_STORE = OFF
GO
USE [SE1715_G4_SWP391]
GO
/****** Object:  Table [dbo].[Administrator]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Administrator](
	[administratorID] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](100) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[lastLogin] [datetime] NULL,
	[isActive] [bit] NOT NULL,
	[roleID] [int] NOT NULL,
	[email] [varchar](100) NULL,
	[img] [varbinary](max) NULL,
 CONSTRAINT [PK_Administrator] PRIMARY KEY CLUSTERED 
(
	[administratorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Attribute]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attribute](
	[attributeID] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](MAX) NULL,
	[name] [nvarchar](MAX) NULL,
	[description] [ntext] NULL,
	[productId] [int] NULL,
 CONSTRAINT [PK_Attribute] PRIMARY KEY CLUSTERED 
(
	[attributeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[blogId] [int] IDENTITY(1,1) NOT NULL,
	code VARCHAR(max),
	[adminId] [int] NULL,
	[typeBlogId] [int] NOT NULL,
	[datePost] [datetime] NOT NULL,
	[lastChange] [datetime] NOT NULL,
	[status] [bit] NOT NULL,
	[title] [ntext] NULL,
	[headerContent] [ntext] NULL,
	[quoteContent] [ntext] NULL,
	[bodyContent] [ntext] NULL,
	[footerContent] [ntext] NULL,
	[img] VARBINARY(max) NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[blogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[categoryID] [int] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](MAX) NOT NULL,
	[name] [nvarchar](MAX) NULL,
	[keyword] [nvarchar](MAX) NULL,
	[description] [ntext] NULL,
	[createdDate] DATETIME,
	[createdBy] INT,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[categoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[commentId] [int] IDENTITY(1,1) NOT NULL,
	[blogId] [int] NOT NULL,
	[content] [ntext] NOT NULL,
	[datePost] [datetime] NOT NULL,
	[customerId] [int] NOT NULL,
	[status] [bit] NOT NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[commentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[customerID] [int] IDENTITY(1,1) NOT NULL,
	[googleId] [varchar](max) NULL,
	[userName] [varchar](100) NULL,
	[password] [varchar](100) NULL,
	[created] [datetime] NULL,
	[lastLogin] [datetime] NULL,
	[status] [bit] NULL,
	[gender] [bit] NULL,
	[image] [varbinary](max) NULL,
	[firstName] [nvarchar](MAX) NULL,
	[lastName] [nvarchar](MAX) NULL,
	[address] [nvarchar](MAX) NULL,
	[phone] [varchar](100) NULL,
	[email] [varchar](100) NULL,
	[totalMoney] [money] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[customerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Discount]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Discount](
	[discountID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](MAX) NULL,
	[description] [ntext] NULL,
	[isActive] [bit] NOT NULL,
	[discount] [real] NOT NULL,
	[couponCode] [varchar](100) NULL,
	[startDate] DATETIME NULL,
	[expirationDate] DATETIME NULL,
	createdDate DATETIME,
	createdBy INT,
 CONSTRAINT [PK_Discount] PRIMARY KEY CLUSTERED 
(
	[discountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[feedbackId] [int] IDENTITY(1,1) NOT NULL,
	[name] NVARCHAR(200),
	[subject] NVARCHAR(200),
	[email] [nvarchar](100) NULL,
	[content] [ntext] NULL,
	[reply] [ntext] NULL,
	[repDate] [datetime] NULL,
	[sendDate] [datetime] NULL,
	[status] [bit] NOT NULL,
	repBy INT,
	[roleId] INT,
 CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED 
(
	[feedbackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[orderID] [int] IDENTITY(1,1) NOT NULL,
	[customerID] [int] NULL,
	[trackingNumber] [int],
	[totalMoney] [money] NULL,
	[orderDate] [datetime] NOT NULL,
	[shippedDate] [datetime] NULL,
	[requiredDate] [datetime] NOT NULL,
	[shipAddress] [nvarchar](200) NOT NULL,
	[status] [int] NULL,
	[shipVia] [int] NULL,
	payment int,
	[notes] [ntext] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[orderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[orderID] [int] NOT NULL,
	[productID] [int] NOT NULL,
	[quantity] [int] NULL,
	[unitPrice] [money] NULL,
	warranty INT, 
	[discount] [real] NOT NULL,
 CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[orderID] ASC,
	[productID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[productID] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](MAX) NOT NULL,
	[name] [nvarchar](MAX) NULL,
	[keywords] [nvarchar](MAX) NULL,
	[shortDescription] [nvarchar](MAX) NULL,
	[description] [ntext] NULL,
	[categoryID] [int] NULL,
	[supplierId] [int] NULL,
	[isActive] [bit] NOT NULL,
	[unitPrice] [money] NULL,
	[image] VARBINARY(max) NULL,
	[stockQuantity] [int] NULL,
	[unitOnOrders] [int] NULL,
	[createdDate] [datetime] NOT NULL,
	[createdBy] [int] NULL,
	[discountId] [int] NULL,
	warranty INT, 
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[productID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductImg]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductImg](
	[ProductImgId] [INT] IDENTITY(1,1) NOT NULL,
	[productID] [int] NOT NULL,
	[image] VARBINARY(MAX) NULL,
	[createdDate] [datetime] NOT NULL,
	[createdBy] [int] NULL,
 CONSTRAINT [PK_ProductImg] PRIMARY KEY CLUSTERED 
(
	[productID] ASC,
	[ProductImgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Review]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Review](
	[reviewId] [int] IDENTITY(1,1) NOT NULL,
	[customerId] [int] NOT NULL,
	[productId] [int] NOT NULL,
	[rate] [int] NOT NULL,
	[content] [ntext] NULL,
	[dateRate] [datetime] NOT NULL,
	[status] [bit] NOT NULL,
 CONSTRAINT [PK_Review] PRIMARY KEY CLUSTERED 
(
	[reviewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[roleID] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](100) NULL,
	[name] [nvarchar](MAX) NULL,
	[description] [ntext] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shipments]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipments](
	[shipmentID] [int] IDENTITY(1,1) NOT NULL,
	[companyName] [nvarchar](MAX) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[phone] [nvarchar](20) NOT NULL,
	[status] [bit] NULL,
	[createdDate] [datetime] NULL,
	createdBy INT,
 CONSTRAINT [PK_Shipments] PRIMARY KEY CLUSTERED 
(
	[shipmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SlideShow]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SlideShow](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[image] VARBINARY(max) NULL,
	[disable] [bit] NULL,
	[createBy] [int] NULL,
	[createDate] [datetime] NULL,
	[slogan] [ntext] NULL,
 CONSTRAINT [PK_SlideShow] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[supplierId] [int] IDENTITY(1,1) NOT NULL,
	[companyName] [nvarchar](MAX) NULL,
	status [bit] NOT NULL,
	[createdDate] [datetime] NOT NULL,
	[createdBy] [int] NULL,
	[email] [varchar](100) NULL,
	[phone] [varchar](20) NULL,
	[homePage] [varchar](200) NULL,
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[supplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TypeBlog]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeBlog](
	[typeId] [int] IDENTITY(1,1) NOT NULL,
	code VARCHAR(MAX), 
	[typeName] [nvarchar](MAX) NOT NULL,
	createdDate DATETIME,
	createdBy INT,
 CONSTRAINT [PK_TypeBlog] PRIMARY KEY CLUSTERED 
(
	[typeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Warranty]    Script Date: 5/17/2023 3:32:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Warranty](
	[warrantyID] [int] IDENTITY(1,1) NOT NULL,
	[productID] [int] NOT NULL,
	[code] [varchar](MAX) NOT NULL,
	[name] [nvarchar](MAX) NULL,
	[description] [nvarchar](MAX ) NULL,
	[isActive] [bit] NOT NULL,
	[createdDate] [datetime] NULL,
	[timesUsed] [int] NULL,
 CONSTRAINT [PK_Warranty] PRIMARY KEY CLUSTERED 
(
	[warrantyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Administrator]  WITH CHECK ADD  CONSTRAINT [FK_Administrator_Role] FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([roleID])
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_OrderId_TrackingNumber] FOREIGN KEY([trackingNumber])
REFERENCES [dbo].[order] ([orderID])
GO
ALTER TABLE [dbo].[Administrator] CHECK CONSTRAINT [FK_Administrator_Role]
GO
ALTER TABLE [dbo].[Attribute]  WITH CHECK ADD  CONSTRAINT [FK_Attribute_Product] FOREIGN KEY([productId])
REFERENCES [dbo].[Product] ([productID])
GO
ALTER TABLE [dbo].[Attribute] CHECK CONSTRAINT [FK_Attribute_Product]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_Administrator] FOREIGN KEY([adminId])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_Administrator]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_TypeBlog] FOREIGN KEY([typeBlogId])
REFERENCES [dbo].[TypeBlog] ([typeId])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_TypeBlog]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Blog] FOREIGN KEY([blogId])
REFERENCES [dbo].[Blog] ([blogId])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_Blog]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Custmer] FOREIGN KEY([customerId])
REFERENCES [dbo].[Customer] ([customerID])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_Custmer]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customer] FOREIGN KEY([customerID])
REFERENCES [dbo].[Customer] ([customerID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Customer]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Shipments] FOREIGN KEY([shipVia])
REFERENCES [dbo].[Shipments] ([shipmentID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Shipments]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetails_Order] FOREIGN KEY([orderID])
REFERENCES [dbo].[Order] ([orderID])
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [FK_OrderDetails_Order]
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetails_Product] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([productID])
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_Role] FOREIGN KEY([roleId])
REFERENCES [dbo].[role] ([roleId])
GO
ALTER TABLE [dbo].[OrderDetails] CHECK CONSTRAINT [FK_OrderDetails_Product]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Administrator] FOREIGN KEY([createdBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Administrator]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([categoryID])
REFERENCES [dbo].[Category] ([categoryID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Category]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Discount] FOREIGN KEY([discountId])
REFERENCES [dbo].[Discount] ([discountID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Discount]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Supplier] FOREIGN KEY([supplierId])
REFERENCES [dbo].[Supplier] ([supplierId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Supplier]
GO
ALTER TABLE [dbo].[ProductImg]  WITH CHECK ADD  CONSTRAINT [FK_ProductImg_Administrator] FOREIGN KEY([createdBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[ProductImg] CHECK CONSTRAINT [FK_ProductImg_Administrator]
GO
ALTER TABLE [dbo].[ProductImg]  WITH CHECK ADD  CONSTRAINT [FK_ProductImg_Product] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([productID])
GO
ALTER TABLE [dbo].[ProductImg] CHECK CONSTRAINT [FK_ProductImg_Product]
GO
ALTER TABLE [dbo].[Review]  WITH CHECK ADD  CONSTRAINT [FK_Review_Customer] FOREIGN KEY([customerId])
REFERENCES [dbo].[Customer] ([customerID])
GO
ALTER TABLE [dbo].[Review] CHECK CONSTRAINT [FK_Review_Customer]
GO
ALTER TABLE [dbo].[Review]  WITH CHECK ADD  CONSTRAINT [FK_Review_Product] FOREIGN KEY([productId])
REFERENCES [dbo].[Product] ([productID])
GO
ALTER TABLE [dbo].[Review] CHECK CONSTRAINT [FK_Review_Product]
GO
ALTER TABLE [dbo].[SlideShow]  WITH CHECK ADD  CONSTRAINT [FK_SlideShow_Administrator] FOREIGN KEY([createBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[SlideShow] CHECK CONSTRAINT [FK_SlideShow_Administrator]
GO
ALTER TABLE [dbo].[Supplier]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_Administrator] FOREIGN KEY([createdBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_Administrator] FOREIGN KEY([createdBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_Administrator] FOREIGN KEY([repBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[TypeBlog]  WITH CHECK ADD  CONSTRAINT [FK_TypeBlog_Administrator] FOREIGN KEY([createdBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[Discount]  WITH CHECK ADD  CONSTRAINT [FK_Discount_Administrator] FOREIGN KEY([createdBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[Shipments]  WITH CHECK ADD  CONSTRAINT [FK_Shipments_Administrator] FOREIGN KEY([createdBy])
REFERENCES [dbo].[Administrator] ([administratorID])
GO
ALTER TABLE [dbo].[Supplier] CHECK CONSTRAINT [FK_Supplier_Administrator]
GO
ALTER TABLE [dbo].[Warranty]  WITH CHECK ADD  CONSTRAINT [FK_Warranty_Product] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([productID])
GO
ALTER TABLE [dbo].[Warranty] CHECK CONSTRAINT [FK_Warranty_Product]
GO
USE [master]
GO
ALTER DATABASE [SE1715_G4_SWP391] SET  READ_WRITE 
GO
